package com.piserver.piserver;

import java.beans.Statement;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
//import java.sql.Date;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.BreakIterator;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.logging.LogManager;

import javax.validation.constraints.Size;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.awt.Desktop;
import java.net.URI;
import java.net.URISyntaxException;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.JsonParserFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/run")
@Configuration
@EnableScheduling
public class Controller {

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)

    // Configuración del Schedule para que se ejecute cada un minuto
    @Scheduled(fixedRate = 7200000) // Configuración del Schedule para que se ejecute cada 10 minutos (600000 milisegundos)

    public void get() throws FileNotFoundException, InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException, ParseException {

        java.util.Date date = new java.util.Date(); // FECHA CON FORMATO POR DEFECTO
        SimpleDateFormat myformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); // SE SETEA FORMATO DE FECHA
        String date_yyyymmdd = myformat.format(date); // FECHA ACTUAL CON FORMATO yyyy-MM-dd hh:mm:ss
        Calendar c = Calendar.getInstance();
        c.add(Calendar.MINUTE, -5); //
        String five_min_less = myformat.format(c.getTime());

        String feInicial = "2020-12-18 12:00:00"; // fecha de extracción inicial
        String feFinal = "2021-11-22 12:00:00"; // fecha de extracción final
        java.util.Date fechaParsed = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(feInicial); // formateo de fecha
        Calendar h = Calendar.getInstance();
        h.setTime(fechaParsed);
        h.add(Calendar.DATE, 10); // se suman 15 días para ir buscando nuevos datos hacia PI
        SimpleDateFormat myformat2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String feInicialMasDias = myformat2.format(h.getTime());


        java.sql.Connection con_pi = null;
        String url = "jdbc:pisql://MLPCHARMES/Data Source=MLPFAEPI01; Integrated Security=SSPI;Command Timeout=60000; Connect Timeout = 60000;";
        Properties plist = new Properties();

        plist.put("TrustedConnection", "False");
        plist.put("ProtocolOrder", "Https/Soap:5461,NetTcp:5462");
        plist.put("LogConsole", "True");
        plist.put("LogLevel", "0");
        plist.put("tagNamePattern", "sinusoid");
        plist.put("user", "SANTIAGO/cgsdlugo");
        plist.put("password", "Octubre.2021");

        String driver = "com.osisoft.jdbc.Driver";
        Class.forName(driver).newInstance();
        con_pi = DriverManager.getConnection(url, plist);

        
        PreparedStatement pStatement = null; // statement para hacer SELECT hacia PI
        ResultSet rs_pi = null; 
        
        // conexión a sql server
        Connection con_sql = null;
        String sql = "jdbc:sqlserver://168.232.167.33;database=rmes_engine_test;charset=UTF-8;user=rmes_test;password=rmes1097";
        Properties sql_plist = new Properties();
        con_sql = DriverManager.getConnection(sql, sql_plist);

        // query para obtener la fecha del último registro en picomp2 de sql
        String query_last = "SELECT top 1 * FROM picomp2 order by time desc";
        PreparedStatement stmt_last = con_sql.prepareStatement(query_last);
        ResultSet rs_last = stmt_last.executeQuery();
        
        String last_date = "";
        if (rs_last.next()) {
            last_date = rs_last.getTimestamp("time").toString();
        }

        
        // si el ultimo registro de fecha es menor a la fecha final de extracción y además no es null, se suman 15 días a esa fecha encontrada
        if(last_date.compareTo("2021-11-22 12:00:00") < 0 && last_date.length() > 0){ 
            java.util.Date fechaParsed2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(last_date); // formateo de fecha
            Calendar n = Calendar.getInstance();
            n.setTime(fechaParsed2);
            n.add(Calendar.DATE, 10); // se suman 10 días para ir buscando nuevos datos hacia PI
            feInicialMasDias = myformat2.format(n.getTime());

        }

        //System.out.println("last_date: "+ last_date);

        java.sql.Statement stmt_insert = con_sql.createStatement(); // statement para insert hacia sql

        pStatement = con_pi.prepareStatement("SELECT * FROM piarchive..picomp2 WHERE time >= '2020-12-18 12:00:00' AND time < '2021-11-22 12:00:00' AND tag ='320:L2.F80(inch)' ORDER BY time DESC");
        rs_pi = pStatement.executeQuery();

        int contador = 0;
        int cant_insert = 0;

        String ult_reg = "";
        while (rs_pi.next()) {
            contador++;
            String insert_picomp = "INSERT INTO picomp2 VALUES ('" + rs_pi.getString("tag") + "', '"+ rs_pi.getTimestamp("time").toString() + "', '" + rs_pi.getString("value") + "')";
            stmt_insert.addBatch(insert_picomp);
            
            if(contador == 10000){
                int[] result = stmt_insert.executeBatch();
                stmt_insert.clearBatch();
                contador = 0;
                cant_insert++;
                System.out.println("CANT BATCHS: " + cant_insert + " - Fecha: " + rs_pi.getTimestamp("time").toString());
            }

        }

        if(contador > 0){
            int[] result = stmt_insert.executeBatch();
        }

        //resultSet = null;
        //con_pi.close();







        
        System.out.println("------------------------------ FIN ETL ------------------------------");
    }

}

